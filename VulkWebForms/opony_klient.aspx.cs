﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class opony_klient : System.Web.UI.Page
{
    protected void Page_Init(object sender, EventArgs e)
    {

        SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["vulkdb"].ConnectionString;
    }

    protected void Page_Load(object sender, EventArgs e) {

    }

    protected void addButton_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;

        GridViewRow gvr = (GridViewRow)btn.NamingContainer;
        int row = gvr.DataItemIndex;

        int i = (int)TiresView.DataKeys[row].Value;

        SqlDataSource1.InsertCommand = "Insert into Orders(order_date, delivery_date, quantity, order_cost,tire_id,user_id) Values ('"+DateTime.Now.ToString("yyyy-MM-dd")+"','"+DateTime.Now.AddDays(5).ToString("yyyy-MM-dd")+"',1,15.50,"+i+","+Session["user_id"].ToString()+");";
        SqlDataSource1.Insert();

    }
}