﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="register" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <table style="width: 100%;">
        <tr>
            <td>
                <asp:Label runat="server" Text="Login:"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" AutoCompleteType="Disabled"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" Text="Password:"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" Text="E-Mail:"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBox3" runat="server" AutoCompleteType="Disabled"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2">
                
                <br />


                <asp:DropDownList ID="roleDropdown" runat="server" DataValueField="Id" DataTextField="name">
                </asp:DropDownList>

                <br />
                <asp:Button ID="register_button" runat="server" Text="Register" OnClick="register_button_Click" />
            </td>
        </tr>
    </table>

    </asp:Content>

