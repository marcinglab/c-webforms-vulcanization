﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TiresSupplier.aspx.cs" Inherits="TiresSupplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:VulcanizationConnectionString2 %>" DeleteCommand="DELETE FROM [TireSupplier] WHERE [id] = @original_id AND [name] = @original_name AND [nip] = @original_nip AND [regon] = @original_regon AND [address_id] = @original_address_id" InsertCommand="INSERT INTO [TireSupplier] ([name], [nip], [regon], [address_id]) VALUES (@name, @nip, @regon, @address_id)" SelectCommand="SELECT * FROM [TireSupplier]" UpdateCommand="UPDATE [TireSupplier] SET [name] = @name, [nip] = @nip, [regon] = @regon, [address_id] = @address_id WHERE [id] = @original_id AND [name] = @original_name AND [nip] = @original_nip AND [regon] = @original_regon AND [address_id] = @original_address_id" ConflictDetection="CompareAllValues" OldValuesParameterFormatString="original_{0}">
        <DeleteParameters>
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_name" Type="String" />
            <asp:Parameter Name="original_nip" Type="String" />
            <asp:Parameter Name="original_regon" Type="String" />
            <asp:Parameter Name="original_address_id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="nip" Type="String" />
            <asp:Parameter Name="regon" Type="String" />
            <asp:Parameter Name="address_id" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="name" Type="String" />
            <asp:Parameter Name="nip" Type="String" />
            <asp:Parameter Name="regon" Type="String" />
            <asp:Parameter Name="address_id" Type="Int32" />
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_name" Type="String" />
            <asp:Parameter Name="original_nip" Type="String" />
            <asp:Parameter Name="original_regon" Type="String" />
            <asp:Parameter Name="original_address_id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>

    
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="id" DataSourceID="SqlDataSource1" DefaultMode="Insert">
        <EditItemTemplate>
            id:
            <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("id") %>' />
            <br />
            name:
            <asp:TextBox ID="nameTextBox" runat="server" Text='<%# Bind("name") %>' />
            <br />
            nip:
            <asp:TextBox ID="nipTextBox" runat="server" Text='<%# Bind("nip") %>' />
            <br />
            regon:
            <asp:TextBox ID="regonTextBox" runat="server" Text='<%# Bind("regon") %>' />
            <br />
            address_id:
            <asp:TextBox ID="address_idTextBox" runat="server" Text='<%# Bind("address_id") %>' />
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Aktualizuj" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Anuluj" />
        </EditItemTemplate>
        <InsertItemTemplate>
            name:
            <asp:TextBox ID="nameTextBox" runat="server" Text='<%# Bind("name") %>' />
            <br />
            nip:
            <asp:TextBox ID="nipTextBox" runat="server" Text='<%# Bind("nip") %>' />
            <br />
            regon:
            <asp:TextBox ID="regonTextBox" runat="server" Text='<%# Bind("regon") %>' />
            <br />
            address_id:
            <asp:TextBox ID="address_idTextBox" runat="server" Text='<%# Bind("address_id") %>' />
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Wstaw" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Anuluj" />
        </InsertItemTemplate>
        <ItemTemplate>
            id:
            <asp:Label ID="idLabel" runat="server" Text='<%# Eval("id") %>' />
            <br />
            name:
            <asp:Label ID="nameLabel" runat="server" Text='<%# Bind("name") %>' />
            <br />
            nip:
            <asp:Label ID="nipLabel" runat="server" Text='<%# Bind("nip") %>' />
            <br />
            regon:
            <asp:Label ID="regonLabel" runat="server" Text='<%# Bind("regon") %>' />
            <br />
            address_id:
            <asp:Label ID="address_idLabel" runat="server" Text='<%# Bind("address_id") %>' />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edytuj" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Usuń" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="Nowy" />
        </ItemTemplate>
    </asp:FormView>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True" SortExpression="id" InsertVisible="False" />
            <asp:BoundField DataField="name" HeaderText="name" SortExpression="name" />
            <asp:BoundField DataField="nip" HeaderText="nip" SortExpression="nip" />
            <asp:BoundField DataField="regon" HeaderText="regon" SortExpression="regon" />
            <asp:BoundField DataField="address_id" HeaderText="address_id" SortExpression="address_id" />
        </Columns>
    </asp:GridView>
    

    
</asp:Content>

