﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Visits.aspx.cs" Inherits="Pages_Admin_Visits" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:VulcanizationConnectionString2 %>" DeleteCommand="DELETE FROM [Visits] WHERE [id] = @original_id AND (([data] = @original_data) OR ([data] IS NULL AND @original_data IS NULL)) AND (([hour] = @original_hour) OR ([hour] IS NULL AND @original_hour IS NULL)) AND [vulc_inst_id] = @original_vulc_inst_id" InsertCommand="INSERT INTO [Visits] ([data], [hour], [vulc_inst_id]) VALUES (@data, @hour, @vulc_inst_id)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Visits]" UpdateCommand="UPDATE [Visits] SET [data] = @data, [hour] = @hour, [vulc_inst_id] = @vulc_inst_id WHERE [id] = @original_id AND (([data] = @original_data) OR ([data] IS NULL AND @original_data IS NULL)) AND (([hour] = @original_hour) OR ([hour] IS NULL AND @original_hour IS NULL)) AND [vulc_inst_id] = @original_vulc_inst_id">
        <DeleteParameters>
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter DbType="Date" Name="original_data" />
            <asp:Parameter Name="original_hour" Type="String" />
            <asp:Parameter Name="original_vulc_inst_id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter DbType="Date" Name="data" />
            <asp:Parameter Name="hour" Type="String" />
            <asp:Parameter Name="vulc_inst_id" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter DbType="Date" Name="data" />
            <asp:Parameter Name="hour" Type="String" />
            <asp:Parameter Name="vulc_inst_id" Type="Int32" />
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter DbType="Date" Name="original_data" />
            <asp:Parameter Name="original_hour" Type="String" />
            <asp:Parameter Name="original_vulc_inst_id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="id" DataSourceID="SqlDataSource1" DefaultMode="Insert">
        <EditItemTemplate>
            id:
            <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("id") %>' />
            <br />
            data:
            <asp:TextBox ID="dataTextBox" runat="server" Text='<%# Bind("data") %>' />
            <br />
            hour:
            <asp:TextBox ID="hourTextBox" runat="server" Text='<%# Bind("hour") %>' />
            <br />
            vulc_inst_id:
            <asp:TextBox ID="vulc_inst_idTextBox" runat="server" Text='<%# Bind("vulc_inst_id") %>' />
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Aktualizuj" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Anuluj" />
        </EditItemTemplate>
        <InsertItemTemplate>
            data:
            <asp:TextBox ID="dataTextBox" runat="server" Text='<%# Bind("data") %>' />
            <br />
            hour:
            <asp:TextBox ID="hourTextBox" runat="server" Text='<%# Bind("hour") %>' />
            <br />
            vulc_inst_id:
            <asp:TextBox ID="vulc_inst_idTextBox" runat="server" Text='<%# Bind("vulc_inst_id") %>' />
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Wstaw" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Anuluj" />
        </InsertItemTemplate>
        <ItemTemplate>
            id:
            <asp:Label ID="idLabel" runat="server" Text='<%# Eval("id") %>' />
            <br />
            data:
            <asp:Label ID="dataLabel" runat="server" Text='<%# Bind("data") %>' />
            <br />
            hour:
            <asp:Label ID="hourLabel" runat="server" Text='<%# Bind("hour") %>' />
            <br />
            vulc_inst_id:
            <asp:Label ID="vulc_inst_idLabel" runat="server" Text='<%# Bind("vulc_inst_id") %>' />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edytuj" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Usuń" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="Nowy" />
        </ItemTemplate>
    </asp:FormView>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="data" HeaderText="data" SortExpression="data" />
            <asp:BoundField DataField="hour" HeaderText="hour" SortExpression="hour" />
            <asp:BoundField DataField="vulc_inst_id" HeaderText="vulc_inst_id" SortExpression="vulc_inst_id" />
        </Columns>
    </asp:GridView>
    <p>
    </p>
</asp:Content>

