﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Users.aspx.cs" Inherits="Pages_Admin_Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:VulcanizationConnectionString2 %>" DeleteCommand="DELETE FROM [User] WHERE [Id] = @original_Id " InsertCommand="INSERT INTO [User] ([login], [password], [email], [id_role], [id_adres]) VALUES (@login, @password, @email, @id_role, @id_adres)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [User]" UpdateCommand="UPDATE [User] SET [login] = @login, [password] = @password, [email] = @email, [id_role] = @id_role, [id_adres] = @id_adres WHERE [Id] = @original_Id AND [login] = @original_login AND [password] = @original_password AND [email] = @original_email AND [id_role] = @original_id_role AND (([id_adres] = @original_id_adres) OR ([id_adres] IS NULL AND @original_id_adres IS NULL))">
        <DeleteParameters>
            <asp:Parameter Name="original_Id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="login" Type="String" />
            <asp:Parameter Name="password" Type="String" />
            <asp:Parameter Name="email" Type="String" />
            <asp:Parameter Name="id_role" Type="Int32" />
            <asp:Parameter Name="id_adres" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="login" Type="String" />
            <asp:Parameter Name="password" Type="String" />
            <asp:Parameter Name="email" Type="String" />
            <asp:Parameter Name="id_role" Type="Int32" />
            <asp:Parameter Name="id_adres" Type="Int32" />
            <asp:Parameter Name="original_Id" Type="Int32" />
            <asp:Parameter Name="original_login" Type="String" />
            <asp:Parameter Name="original_password" Type="String" />
            <asp:Parameter Name="original_email" Type="String" />
            <asp:Parameter Name="original_id_role" Type="Int32" />
            <asp:Parameter Name="original_id_adres" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="Id" DataSourceID="SqlDataSource1" DefaultMode="Insert">
        <EditItemTemplate>
            Id:
            <asp:Label ID="IdLabel1" runat="server" Text='<%# Eval("Id") %>' />
            <br />
            login:
            <asp:TextBox ID="loginTextBox" runat="server" Text='<%# Bind("login") %>' />
            <br />
            password:
            <asp:TextBox ID="passwordTextBox" runat="server" Text='<%# Bind("password") %>' />
            <br />
            email:
            <asp:TextBox ID="emailTextBox" runat="server" Text='<%# Bind("email") %>' />
            <br />
            id_role:
            <asp:TextBox ID="id_roleTextBox" runat="server" Text='<%# Bind("id_role") %>' />
            <br />
            id_adres:
            <asp:TextBox ID="id_adresTextBox" runat="server" Text='<%# Bind("id_adres") %>' />
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Aktualizuj" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Anuluj" />
        </EditItemTemplate>
        <InsertItemTemplate>
            login:
            <asp:TextBox ID="loginTextBox" runat="server" Text='<%# Bind("login") %>' />
            <br />
            password:
            <asp:TextBox ID="passwordTextBox" runat="server" Text='<%# Bind("password") %>' />
            <br />
            email:
            <asp:TextBox ID="emailTextBox" runat="server" Text='<%# Bind("email") %>' />
            <br />
            id_role:
            <asp:TextBox ID="id_roleTextBox" runat="server" Text='<%# Bind("id_role") %>' />
            <br />
            id_adres:
            <asp:TextBox ID="id_adresTextBox" runat="server" Text='<%# Bind("id_adres") %>' />
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Wstaw" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Anuluj" />
        </InsertItemTemplate>
        <ItemTemplate>
            Id:
            <asp:Label ID="IdLabel" runat="server" Text='<%# Eval("Id") %>' />
            <br />
            login:
            <asp:Label ID="loginLabel" runat="server" Text='<%# Bind("login") %>' />
            <br />
            password:
            <asp:Label ID="passwordLabel" runat="server" Text='<%# Bind("password") %>' />
            <br />
            email:
            <asp:Label ID="emailLabel" runat="server" Text='<%# Bind("email") %>' />
            <br />
            id_role:
            <asp:Label ID="id_roleLabel" runat="server" Text='<%# Bind("id_role") %>' />
            <br />
            id_adres:
            <asp:Label ID="id_adresLabel" runat="server" Text='<%# Bind("id_adres") %>' />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edytuj" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Usuń" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="Nowy" />
        </ItemTemplate>
    </asp:FormView>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
            <asp:BoundField DataField="login" HeaderText="login" SortExpression="login" />
            <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" />
            <asp:BoundField DataField="id_role" HeaderText="id_role" SortExpression="id_role" />
            <asp:BoundField DataField="id_adres" HeaderText="id_adres" SortExpression="id_adres" />
            <asp:BoundField DataField="password" HeaderText="password" SortExpression="password" />
        </Columns>
    </asp:GridView>
</asp:Content>

