﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Adres.aspx.cs" Inherits="Pages_Admin_Adres" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:VulcanizationConnectionString2 %>" DeleteCommand="DELETE FROM [Address] WHERE [id] = @original_id AND [town] = @original_town AND [street] = @original_street AND [b_nuber] = @original_b_nuber AND [apt_number] = @original_apt_number" InsertCommand="INSERT INTO [Address] ([town], [street], [b_nuber], [apt_number]) VALUES (@town, @street, @b_nuber, @apt_number)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Address]" UpdateCommand="UPDATE [Address] SET [town] = @town, [street] = @street, [b_nuber] = @b_nuber, [apt_number] = @apt_number WHERE [id] = @original_id AND [town] = @original_town AND [street] = @original_street AND [b_nuber] = @original_b_nuber AND [apt_number] = @original_apt_number">
        <DeleteParameters>
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_town" Type="String" />
            <asp:Parameter Name="original_street" Type="String" />
            <asp:Parameter Name="original_b_nuber" Type="String" />
            <asp:Parameter Name="original_apt_number" Type="String" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="town" Type="String" />
            <asp:Parameter Name="street" Type="String" />
            <asp:Parameter Name="b_nuber" Type="String" />
            <asp:Parameter Name="apt_number" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="town" Type="String" />
            <asp:Parameter Name="street" Type="String" />
            <asp:Parameter Name="b_nuber" Type="String" />
            <asp:Parameter Name="apt_number" Type="String" />
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_town" Type="String" />
            <asp:Parameter Name="original_street" Type="String" />
            <asp:Parameter Name="original_b_nuber" Type="String" />
            <asp:Parameter Name="original_apt_number" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="id" DataSourceID="SqlDataSource1" DefaultMode="Insert">
        <EditItemTemplate>
            id:
            <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("id") %>' />
            <br />
            town:
            <asp:TextBox ID="townTextBox" runat="server" Text='<%# Bind("town") %>' />
            <br />
            street:
            <asp:TextBox ID="streetTextBox" runat="server" Text='<%# Bind("street") %>' />
            <br />
            b_nuber:
            <asp:TextBox ID="b_nuberTextBox" runat="server" Text='<%# Bind("b_nuber") %>' />
            <br />
            apt_number:
            <asp:TextBox ID="apt_numberTextBox" runat="server" Text='<%# Bind("apt_number") %>' />
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Aktualizuj" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Anuluj" />
        </EditItemTemplate>
        <InsertItemTemplate>
            town:
            <asp:TextBox ID="townTextBox" runat="server" Text='<%# Bind("town") %>' />
            <br />
            street:
            <asp:TextBox ID="streetTextBox" runat="server" Text='<%# Bind("street") %>' />
            <br />
            b_nuber:
            <asp:TextBox ID="b_nuberTextBox" runat="server" Text='<%# Bind("b_nuber") %>' />
            <br />
            apt_number:
            <asp:TextBox ID="apt_numberTextBox" runat="server" Text='<%# Bind("apt_number") %>' />
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Wstaw" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Anuluj" />
        </InsertItemTemplate>
        <ItemTemplate>
            id:
            <asp:Label ID="idLabel" runat="server" Text='<%# Eval("id") %>' />
            <br />
            town:
            <asp:Label ID="townLabel" runat="server" Text='<%# Bind("town") %>' />
            <br />
            street:
            <asp:Label ID="streetLabel" runat="server" Text='<%# Bind("street") %>' />
            <br />
            b_nuber:
            <asp:Label ID="b_nuberLabel" runat="server" Text='<%# Bind("b_nuber") %>' />
            <br />
            apt_number:
            <asp:Label ID="apt_numberLabel" runat="server" Text='<%# Bind("apt_number") %>' />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edytuj" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Usuń" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="Nowy" />
        </ItemTemplate>
    </asp:FormView>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="town" HeaderText="town" SortExpression="town" />
            <asp:BoundField DataField="street" HeaderText="street" SortExpression="street" />
            <asp:BoundField DataField="b_nuber" HeaderText="b_nuber" SortExpression="b_nuber" />
            <asp:BoundField DataField="apt_number" HeaderText="apt_number" SortExpression="apt_number" />
        </Columns>
    </asp:GridView>
</asp:Content>

