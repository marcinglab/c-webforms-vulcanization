﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Orders.aspx.cs" Inherits="Pages_Admin_Orders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:VulcanizationConnectionString2 %>" DeleteCommand="DELETE FROM [Orders] WHERE [id] = @original_id AND [order_date] = @original_order_date AND [delivery_date] = @original_delivery_date AND [quantity] = @original_quantity AND [order_cost] = @original_order_cost AND [tire_id] = @original_tire_id AND [user_id] = @original_user_id" InsertCommand="INSERT INTO [Orders] ([order_date], [delivery_date], [quantity], [order_cost], [tire_id], [user_id]) VALUES (@order_date, @delivery_date, @quantity, @order_cost, @tire_id, @user_id)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Orders]" UpdateCommand="UPDATE [Orders] SET [order_date] = @order_date, [delivery_date] = @delivery_date, [quantity] = @quantity, [order_cost] = @order_cost, [tire_id] = @tire_id, [user_id] = @user_id WHERE [id] = @original_id AND [order_date] = @original_order_date AND [delivery_date] = @original_delivery_date AND [quantity] = @original_quantity AND [order_cost] = @original_order_cost AND [tire_id] = @original_tire_id AND [user_id] = @original_user_id">
        <DeleteParameters>
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter DbType="Date" Name="original_order_date" />
            <asp:Parameter DbType="Date" Name="original_delivery_date" />
            <asp:Parameter Name="original_quantity" Type="Int32" />
            <asp:Parameter Name="original_order_cost" Type="Decimal" />
            <asp:Parameter Name="original_tire_id" Type="Int32" />
            <asp:Parameter Name="original_user_id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter DbType="Date" Name="order_date" />
            <asp:Parameter DbType="Date" Name="delivery_date" />
            <asp:Parameter Name="quantity" Type="Int32" />
            <asp:Parameter Name="order_cost" Type="Decimal" />
            <asp:Parameter Name="tire_id" Type="Int32" />
            <asp:Parameter Name="user_id" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter DbType="Date" Name="order_date" />
            <asp:Parameter DbType="Date" Name="delivery_date" />
            <asp:Parameter Name="quantity" Type="Int32" />
            <asp:Parameter Name="order_cost" Type="Decimal" />
            <asp:Parameter Name="tire_id" Type="Int32" />
            <asp:Parameter Name="user_id" Type="Int32" />
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter DbType="Date" Name="original_order_date" />
            <asp:Parameter DbType="Date" Name="original_delivery_date" />
            <asp:Parameter Name="original_quantity" Type="Int32" />
            <asp:Parameter Name="original_order_cost" Type="Decimal" />
            <asp:Parameter Name="original_tire_id" Type="Int32" />
            <asp:Parameter Name="original_user_id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="id" DataSourceID="SqlDataSource1" DefaultMode="Insert">
        <EditItemTemplate>
            id:
            <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("id") %>' />
            <br />
            order_date:
            <asp:TextBox ID="order_dateTextBox" runat="server" Text='<%# Bind("order_date") %>' />
            <br />
            delivery_date:
            <asp:TextBox ID="delivery_dateTextBox" runat="server" Text='<%# Bind("delivery_date") %>' />
            <br />
            quantity:
            <asp:TextBox ID="quantityTextBox" runat="server" Text='<%# Bind("quantity") %>' />
            <br />
            order_cost:
            <asp:TextBox ID="order_costTextBox" runat="server" Text='<%# Bind("order_cost") %>' />
            <br />
            tire_id:
            <asp:TextBox ID="tire_idTextBox" runat="server" Text='<%# Bind("tire_id") %>' />
            <br />
            user_id:
            <asp:TextBox ID="user_idTextBox" runat="server" Text='<%# Bind("user_id") %>' />
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Aktualizuj" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Anuluj" />
        </EditItemTemplate>
        <InsertItemTemplate>
            order_date:
            <asp:TextBox ID="order_dateTextBox" runat="server" Text='<%# Bind("order_date") %>' />
            <br />
            delivery_date:
            <asp:TextBox ID="delivery_dateTextBox" runat="server" Text='<%# Bind("delivery_date") %>' />
            <br />
            quantity:
            <asp:TextBox ID="quantityTextBox" runat="server" Text='<%# Bind("quantity") %>' />
            <br />
            order_cost:
            <asp:TextBox ID="order_costTextBox" runat="server" Text='<%# Bind("order_cost") %>' />
            <br />
            tire_id:
            <asp:TextBox ID="tire_idTextBox" runat="server" Text='<%# Bind("tire_id") %>' />
            <br />
            user_id:
            <asp:TextBox ID="user_idTextBox" runat="server" Text='<%# Bind("user_id") %>' />
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Wstaw" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Anuluj" />
        </InsertItemTemplate>
        <ItemTemplate>
            id:
            <asp:Label ID="idLabel" runat="server" Text='<%# Eval("id") %>' />
            <br />
            order_date:
            <asp:Label ID="order_dateLabel" runat="server" Text='<%# Bind("order_date") %>' />
            <br />
            delivery_date:
            <asp:Label ID="delivery_dateLabel" runat="server" Text='<%# Bind("delivery_date") %>' />
            <br />
            quantity:
            <asp:Label ID="quantityLabel" runat="server" Text='<%# Bind("quantity") %>' />
            <br />
            order_cost:
            <asp:Label ID="order_costLabel" runat="server" Text='<%# Bind("order_cost") %>' />
            <br />
            tire_id:
            <asp:Label ID="tire_idLabel" runat="server" Text='<%# Bind("tire_id") %>' />
            <br />
            user_id:
            <asp:Label ID="user_idLabel" runat="server" Text='<%# Bind("user_id") %>' />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edytuj" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Usuń" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="Nowy" />
        </ItemTemplate>
    </asp:FormView>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="order_date" HeaderText="order_date" SortExpression="order_date" />
            <asp:BoundField DataField="delivery_date" HeaderText="delivery_date" SortExpression="delivery_date" />
            <asp:BoundField DataField="quantity" HeaderText="quantity" SortExpression="quantity" />
            <asp:BoundField DataField="order_cost" HeaderText="order_cost" SortExpression="order_cost" />
            <asp:BoundField DataField="tire_id" HeaderText="tire_id" SortExpression="tire_id" />
            <asp:BoundField DataField="user_id" HeaderText="user_id" SortExpression="user_id" />
        </Columns>
    </asp:GridView>
</asp:Content>

