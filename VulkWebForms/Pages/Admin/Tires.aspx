﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Tires.aspx.cs" Inherits="Pages_Admin_Tires" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:VulcanizationConnectionString2 %>" DeleteCommand="DELETE FROM [Tires] WHERE [id] = @original_id AND [width] = @original_width AND [height] = @original_height AND [diameter] = @original_diameter AND [structure] = @original_structure AND [directionality] = @original_directionality AND [season] = @original_season AND [cost] = @original_cost AND [tiresupplier_id] = @original_tiresupplier_id" InsertCommand="INSERT INTO [Tires] ([width], [height], [diameter], [structure], [directionality], [season], [cost], [tiresupplier_id]) VALUES (@width, @height, @diameter, @structure, @directionality, @season, @cost, @tiresupplier_id)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT * FROM [Tires]" UpdateCommand="UPDATE [Tires] SET [width] = @width, [height] = @height, [diameter] = @diameter, [structure] = @structure, [directionality] = @directionality, [season] = @season, [cost] = @cost, [tiresupplier_id] = @tiresupplier_id WHERE [id] = @original_id AND [width] = @original_width AND [height] = @original_height AND [diameter] = @original_diameter AND [structure] = @original_structure AND [directionality] = @original_directionality AND [season] = @original_season AND [cost] = @original_cost AND [tiresupplier_id] = @original_tiresupplier_id">
        <DeleteParameters>
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_width" Type="String" />
            <asp:Parameter Name="original_height" Type="String" />
            <asp:Parameter Name="original_diameter" Type="String" />
            <asp:Parameter Name="original_structure" Type="String" />
            <asp:Parameter Name="original_directionality" Type="String" />
            <asp:Parameter Name="original_season" Type="String" />
            <asp:Parameter Name="original_cost" Type="Decimal" />
            <asp:Parameter Name="original_tiresupplier_id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="width" Type="String" />
            <asp:Parameter Name="height" Type="String" />
            <asp:Parameter Name="diameter" Type="String" />
            <asp:Parameter Name="structure" Type="String" />
            <asp:Parameter Name="directionality" Type="String" />
            <asp:Parameter Name="season" Type="String" />
            <asp:Parameter Name="cost" Type="Decimal" />
            <asp:Parameter Name="tiresupplier_id" Type="Int32" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="width" Type="String" />
            <asp:Parameter Name="height" Type="String" />
            <asp:Parameter Name="diameter" Type="String" />
            <asp:Parameter Name="structure" Type="String" />
            <asp:Parameter Name="directionality" Type="String" />
            <asp:Parameter Name="season" Type="String" />
            <asp:Parameter Name="cost" Type="Decimal" />
            <asp:Parameter Name="tiresupplier_id" Type="Int32" />
            <asp:Parameter Name="original_id" Type="Int32" />
            <asp:Parameter Name="original_width" Type="String" />
            <asp:Parameter Name="original_height" Type="String" />
            <asp:Parameter Name="original_diameter" Type="String" />
            <asp:Parameter Name="original_structure" Type="String" />
            <asp:Parameter Name="original_directionality" Type="String" />
            <asp:Parameter Name="original_season" Type="String" />
            <asp:Parameter Name="original_cost" Type="Decimal" />
            <asp:Parameter Name="original_tiresupplier_id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:FormView ID="FormView1" runat="server" DataKeyNames="id" DataSourceID="SqlDataSource1" DefaultMode="Insert">
        <EditItemTemplate>
            id:
            <asp:Label ID="idLabel1" runat="server" Text='<%# Eval("id") %>' />
            <br />
            width:
            <asp:TextBox ID="widthTextBox" runat="server" Text='<%# Bind("width") %>' />
            <br />
            height:
            <asp:TextBox ID="heightTextBox" runat="server" Text='<%# Bind("height") %>' />
            <br />
            diameter:
            <asp:TextBox ID="diameterTextBox" runat="server" Text='<%# Bind("diameter") %>' />
            <br />
            structure:
            <asp:TextBox ID="structureTextBox" runat="server" Text='<%# Bind("structure") %>' />
            <br />
            directionality:
            <asp:TextBox ID="directionalityTextBox" runat="server" Text='<%# Bind("directionality") %>' />
            <br />
            season:
            <asp:TextBox ID="seasonTextBox" runat="server" Text='<%# Bind("season") %>' />
            <br />
            cost:
            <asp:TextBox ID="costTextBox" runat="server" Text='<%# Bind("cost") %>' />
            <br />
            tiresupplier_id:
            <asp:TextBox ID="tiresupplier_idTextBox" runat="server" Text='<%# Bind("tiresupplier_id") %>' />
            <br />
            <asp:LinkButton ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Aktualizuj" />
            &nbsp;<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Anuluj" />
        </EditItemTemplate>
        <InsertItemTemplate>
            width:
            <asp:TextBox ID="widthTextBox" runat="server" Text='<%# Bind("width") %>' />
            <br />
            height:
            <asp:TextBox ID="heightTextBox" runat="server" Text='<%# Bind("height") %>' />
            <br />
            diameter:
            <asp:TextBox ID="diameterTextBox" runat="server" Text='<%# Bind("diameter") %>' />
            <br />
            structure:
            <asp:TextBox ID="structureTextBox" runat="server" Text='<%# Bind("structure") %>' />
            <br />
            directionality:
            <asp:TextBox ID="directionalityTextBox" runat="server" Text='<%# Bind("directionality") %>' />
            <br />
            season:
            <asp:TextBox ID="seasonTextBox" runat="server" Text='<%# Bind("season") %>' />
            <br />
            cost:
            <asp:TextBox ID="costTextBox" runat="server" Text='<%# Bind("cost") %>' />
            <br />
            tiresupplier_id:
            <asp:TextBox ID="tiresupplier_idTextBox" runat="server" Text='<%# Bind("tiresupplier_id") %>' />
            <br />
            <asp:LinkButton ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Wstaw" />
            &nbsp;<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Anuluj" />
        </InsertItemTemplate>
        <ItemTemplate>
            id:
            <asp:Label ID="idLabel" runat="server" Text='<%# Eval("id") %>' />
            <br />
            width:
            <asp:Label ID="widthLabel" runat="server" Text='<%# Bind("width") %>' />
            <br />
            height:
            <asp:Label ID="heightLabel" runat="server" Text='<%# Bind("height") %>' />
            <br />
            diameter:
            <asp:Label ID="diameterLabel" runat="server" Text='<%# Bind("diameter") %>' />
            <br />
            structure:
            <asp:Label ID="structureLabel" runat="server" Text='<%# Bind("structure") %>' />
            <br />
            directionality:
            <asp:Label ID="directionalityLabel" runat="server" Text='<%# Bind("directionality") %>' />
            <br />
            season:
            <asp:Label ID="seasonLabel" runat="server" Text='<%# Bind("season") %>' />
            <br />
            cost:
            <asp:Label ID="costLabel" runat="server" Text='<%# Bind("cost") %>' />
            <br />
            tiresupplier_id:
            <asp:Label ID="tiresupplier_idLabel" runat="server" Text='<%# Bind("tiresupplier_id") %>' />
            <br />
            <asp:LinkButton ID="EditButton" runat="server" CausesValidation="False" CommandName="Edit" Text="Edytuj" />
            &nbsp;<asp:LinkButton ID="DeleteButton" runat="server" CausesValidation="False" CommandName="Delete" Text="Usuń" />
            &nbsp;<asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="Nowy" />
        </ItemTemplate>
    </asp:FormView>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
            <asp:BoundField DataField="width" HeaderText="width" SortExpression="width" />
            <asp:BoundField DataField="height" HeaderText="height" SortExpression="height" />
            <asp:BoundField DataField="diameter" HeaderText="diameter" SortExpression="diameter" />
            <asp:BoundField DataField="structure" HeaderText="structure" SortExpression="structure" />
            <asp:BoundField DataField="directionality" HeaderText="directionality" SortExpression="directionality" />
            <asp:BoundField DataField="season" HeaderText="season" SortExpression="season" />
            <asp:BoundField DataField="cost" HeaderText="cost" SortExpression="cost" />
            <asp:BoundField DataField="tiresupplier_id" HeaderText="tiresupplier_id" SortExpression="tiresupplier_id" />
        </Columns>
    </asp:GridView>
</asp:Content>

