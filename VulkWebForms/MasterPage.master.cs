﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session["role"] != null) {
            switch (Session["role"].ToString())
            {
                case "Admin": MultiView_menu.SetActiveView(Admin_menu); break;
                case "Dostawca": MultiView_menu.SetActiveView(Dostawca_menu); break;
                case "Klient": MultiView_menu.SetActiveView(Client_menu); break;
                default :  MultiView_menu.SetActiveView(None_menu); break;
            }
        }
        else
        {
            MultiView_menu.SetActiveView(None_menu);
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
