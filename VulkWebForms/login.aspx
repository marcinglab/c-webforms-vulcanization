﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <table style="width: 100%;">
        <tr>
            <td>
                <asp:Label runat="server" Text="Login:"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" AutoCompleteType="Disabled"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" Text="Password:"></asp:Label></td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="login_button" runat="server" Text="login" OnClick="login_button_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="error" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    
</asp:Content>

